# 元组,不仅仅是不可变的列表   除了用作不可变的列表,它还可以用于没有字段名的记录

# 元组和记录
lax_coordinates = (33.9425, -118.408056)
city, year, pop, chg, area = ("Tokyo", 2003, 32450, 0.66, 8014)
traveler_ids = [("USA", "31195855"), ("BRA", "CE342567"), ("ESP", "XDA205856")]
for passport in sorted(traveler_ids):
    print("%s/%s" % passport)

# for循环可以分别提取元组里的元素,也叫做拆包.拆包让元组可以完美的被当作记录来使用.
for country, _ in traveler_ids:
    print(country)

# 元组拆包
# 平行赋值
latitude, longitude = lax_coordinates
# 另一个很优雅的写法,当属不使用中间变量交换两个变量的值
latitude, longitude = longitude, latitude
# 还可以用*运算符把一个可迭代对象拆开作为函数的参数
print(divmod(20, 8))  # 求商和余数
t = (20, 8)
print(divmod(*t))
q, r = divmod(*t)
print(q, r)

import os
# 对于我们不关心的数据,_占位符能帮助处理这种情况
_, filename = os.path.split("E:\\Project\\FluentPython\\Chap2\\demo1.py")
print(filename)

# 用*来处理剩下的元素
# 在平行赋值中,*前缀只能用在一个变量名的前面,但是这个变量可以出现在赋值表达式的任意位置.
a, b, *rest = range(5)

# 嵌套元组拆包
metro_areas = [
    ("Tokyo", "JP", 36.933, (35.689722, 139.691667)),
    ("Delhi NCR", "IN", 36.933, (35.689722, 139.691667)),
    ("Mexico City", "MX", 36.933, (35.689722, 139.691667)),
    ("New York-Newark", "US", 36.933, (35.689722, 139.691667)),
    ("Sao Paulo", "BR", 36.933, (35.689722, 139.691667)),
]
print("{:15} | {:^9} | {:^9}".format("", "lat.", "long."))
fmt = "{:15} | {:9.4f} | {:9.4f}"   # 强大的format
for name, cc, pop, (latitude, longitude) in metro_areas:
    if longitude >= 0:
        print(fmt.format(name, latitude, longitude))


# 具名元组
from collections import namedtuple
# collections.namedtuple是一个工厂函数,它可以用来构建一个带字段名的元组和一个有名字的类

# 定义和使用具名元组
# 创一个具名元组需要两个参数,一个是类名,另一个是类的各个字段的名字.后者可以是由数个字符串组成的可迭代对象,或者是由空格分隔开的字段名组成的字符串.
City = namedtuple("City", "name country pop coordinates")
# 存放在对应字段里的数据要以一串参数的形式传入到构造函数中.
tokyo = City("Tokyo", "JP", 36.933, (35.689722, 139.691667))
tokyo_ = tuple(("Tokyo", "JP", 36.933, (35.689722, 139.691667)))  # 注意,元组的构造函数却只接受单一的可迭代对象
print(tokyo)
print(tokyo_)
# 可以通过字段名或者位置来获取一个字段的信息
print(tokyo.pop)
print(tokyo[2])

# 除了从普通元组那里继承来的属性外,具名元组还有一些自己专有的属性.
print(City._fields)  # _fields属性是一个包含这个类所有字段名称的元组
LatLong = namedtuple("LatLong", "lat long")
delhi_data = ("Delhi NCR", "IN", 36.933, LatLong(35.689722, 139.691667))
# 用_make()通过接受一个可迭代对象来生成这个类的一个实例,它的作用和City(*delhi_data)是一样的
delhi = City._make(delhi_data)
# _asdict()把具名元组以collections.OrderedDict的形式返回,我们可以利用它来把元组里的信息友好地呈现出来
print(delhi._asdict())


# 作为不可变列表的元组
# 除了增减相关的方法以外,元组支持列表的所有其他方法.
# 还有一个例外是__reversed__方法,但是这个方法只是个优化而已,reversed()在没有__reversed__的情况下也是合法的.
