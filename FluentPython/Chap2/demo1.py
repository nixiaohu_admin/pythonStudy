# 列表推导式同filter和map的比较

symbols = "symbols"
beyond_ascii = [ord(s) for s in symbols if ord(s) > 100]

beyond_ascii_2 = list(filter(lambda c: c > 100, map(ord, symbols)))
print(beyond_ascii)
print(beyond_ascii_2)

# 列表推导式构建笛卡尔积
colors = ["黑色", "白色"]
sizes = ["S", 'M', "L"]
ths = [(color, size) for color in colors for size in sizes]
print(ths)

# 生成器表达式
# 虽然也可以用列表推导式来初始化元组 数组或其他序列类型,但是生成器表达式是更好的选择.这是因为生成器表达式遵循了迭代器协议,
# 可以逐个的产出元素,而不是先建立一个完整的列表,然后再把这个列表传递到某个构造函数里.
# 用生成器表达式初始化元组和数组

print(tuple(ord(s) for s in symbols))  # 注意,如果生成器表达式是一个函数调用过程中的唯一参数,那么不需要额外的再用括号把它围起来.

import array

print(array.array("I", (ord(s) for s in symbols)))  # array的构造方法需要两个参数,因此括号是必须的.array构造方法的第一个参数指定了数组中数字的存储方式.
