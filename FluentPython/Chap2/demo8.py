# 双向队列和其他形式的队列

# 利用.append和.pop方法,我们可以把列表当作栈或者队列来使用.但是删除列表的第一个元素,或者是在第一个元素之前添加一个元素之类的操作是很耗时的,
# 因为这些操作会牵扯到移动列表里的所有元素.

# collections.deque类(双端队列)是一个线程安全,可以快速从两端添加或者删除元素的数据类型

from collections import deque

# 1.maxlen是一个可选参数,代表这个队列可以容纳的元素的数量,而且一旦设定,这个属性就不能修改了
dq = deque(range(10), maxlen=10)
print(dq)  # deque([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], maxlen=10)
# 2.队列的旋转操作接受一个参数n,当n>0时,队列的最右边的n个元素会被移动到队伍的左边.当n<0时,最左边的n个元素会被移动到右边.
dq.rotate(3)
print(dq)  # deque([7, 8, 9, 0, 1, 2, 3, 4, 5, 6], maxlen=10)
dq.rotate(-4)
print(dq)  # deque([1, 2, 3, 4, 5, 6, 7, 8, 9, 0], maxlen=10)
# 3.当试图对一个已满的队列做头部添加操作的时候,它尾部的元素会被删除掉
dq.appendleft(-1)
print(dq)  # deque([-1, 1, 2, 3, 4, 5, 6, 7, 8, 9], maxlen=10)
# 4.在尾部添加3个元素的操作会挤掉-1,1和2
dq.extend([11, 22, 33])
print(dq)  # deque([3, 4, 5, 6, 7, 8, 9, 11, 22, 33], maxlen=10)
# 5.extendleft(iter)方法会把迭代器里的元素逐个添加到双向队列的左边,因此迭代器里的元素会逆序出现在队列里.
dq.extendleft([10, 20, 30, 40])
print(dq)  # deque([40, 30, 20, 10, 3, 4, 5, 6, 7, 8], maxlen=10)
import this
print(this)