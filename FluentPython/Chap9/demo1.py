﻿# -*- coding: utf-8 -*-
# ------------------------------------再谈向量类------------------------------------
from array import array
import math


class Vector2d:
    # type_code是类属性，在Vector2d实例和字节序列之间转换时使用
    type_code = 'd'

    def __init__(self, x, y):
        # 尽早捕捉错误，以防调用Vector2d函数时传入不当参数
        # 使用两个前导下划线把属性标记为私有的
        self.__x = float(x)
        self.__y = float(y)

    # @property装饰器把读值方式标记为特性，读值方法与公开属性同名，都是x
    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def __hash__(self):
        # 我们让向量不可变是有原因的，因为这样才能实现__hash__方法。这个方法应该返回一个整数
        # 理想状态下还要考虑对象属性的散列值（__eq__方法也要使用），因为相等的对象应该具有相同的散列值
        # 最好使用位运算符异或(^)混合各分量的散列值
        return hash(self.x) ^ hash(self.y)

    def __iter__(self):
        # 把Vector2d实例变成可迭代的对象，这样才能拆包
        return (i for i in (self.x, self.y))

    def __repr__(self):
        # 使用{!r}获取各个分量的表示形式，然后插值，构成一个字符串；因为Vector2d是可迭代对象，所以*self会把x和y分量提供给format函数
        class_name = type(self).__name__
        return "{}({!r}, {!r})".format(class_name, *self)

    def __str__(self):
        # 从可抵达的Vector2d对象中可以轻松的得到一个元组，显示为一个有序对
        return str(tuple(self))

    def __bytes__(self):
        return bytes([ord(self.type_code)]) + bytes(array(self.type_code, self))

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __abs__(self):
        return math.hypot(self.x, self.y)

    def __bool__(self):
        return bool(abs(self))

    def __format__(self, format_spec):
        # # 使用内置的format函数把format_spec应用到向量的各个分量上，构建一个可迭代的格式化字符串
        # components = (format(c, format_spec) for c in self)
        # # 把格式化字符串代入公式‘（x，y）’中
        # return "({}, {})".format(*components)

        # 增强__format__方法，计算极坐标
        if format_spec.endswith('p'):  # 如果格式代码以p结尾，使用极坐标
            format_spec = format_spec[:-1]  # 从format_spec中删除p后缀
            coords = (abs(self), self.angle())  # 构建一个元组，表示极坐标
            outer_fmt = "<{}, {}>"  # 把外层格式设为一对尖括号
        else:
            coords = self  # 如果不是以p结尾，使用self的x和y分量构建直接坐标
            outer_fmt = "({}, {})"  # 把外层格式设为一对圆括号
        components = (format(c, format_spec) for c in coords)  # 使用各个分量生成可迭代的对象，构成格式化字符串
        return outer_fmt.format(*components)  # 把格式化字符串带入外层格式

    def angle(self):
        # 定义一个简单的angle方法，使用math.atan2()函数计算角度
        return math.atan2(self.y, self.x)

    @classmethod
    def frombytes(cls, octets):
        type_code = chr(octets[0])
        memv = memoryview(octets[1:]).cast(type_code)
        return cls(*memv)
