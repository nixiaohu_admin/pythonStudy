# ------------------------------------使用一等函数实现设计模式------------------------------------
# ------------------------------------重构'策略'模式------------------------------------
"""
如果合理利用作为一等对象的函数,某些设计模式可以简化.'策略'模式就是其中一个很好的例子.
"""
# ------------------------------------使用一等函数实现设计模式------------------------------------
"""
'策略'模式:定义一系列算法,把它们一一封装起来,并且使它们可以相互替换.本模式使得算法可以独立于使用它的客户而变化.
电商领域有个功能很明显可以使用'策略'模式,即根据客户的属性或订单中的商品计算折扣.假如一个网店制定了下述折扣规则:
.有1000或以上积分的顾客,每个订单享5%折扣.
.同一订单中,单个商品的数量达到20个或以上,享10%折扣.
.订单中不同的商品达到10个或以上,享7%折扣.
简单起见,我们假定一个订单一次只能享用一个折扣.
'策略'模式涉及下列内容:
1.上下文:
    把一些计算委托给'实现不同算法的可互换组件',它提供服务.在这个电商示例中,上下文是Order,它会根据不同的算法计算促销折扣.
2.策略:
    实现不同算法的组件的共同接口,在这个示例中,名为Promotion的抽象类扮演这个角色.
3.具体策略:
    '策略'的具体子类.
具体策略由上下文类的客户选择,在这个示例中,实例化订单之前,系统会以某种方式选择一种促销折扣策略,然后把它传给Order构造方法.
具体怎么选择策略,不在这个模式的职责范围内.
"""
from abc import ABC, abstractmethod
from collections import namedtuple

Customer = namedtuple('Customer', 'name fidelity')


class LineItem:
    def __init__(self, product, quantity, price):
        self.product = product
        self.quantity = quantity
        self.price = price

    def total(self):
        return self.price * self.quantity


class Order:  # 上下文
    def __init__(self, customer: Customer, cart, promotion=None):
        self.__total = None
        self.customer = customer
        self.cart = list(cart)
        self.promotion = promotion

    def total(self):
        if self.__total is None:
            self.__total = sum(item.total() for item in self.cart)
        return self.__total

    def due(self):
        if self.promotion is None:
            discount = 0
        else:
            discount = self.promotion.discount(self)
        return self.__total - discount

    def __repr__(self):
        fmt = '<Order total: {:.2f} due: {:.2f}>'
        return fmt.format(self.total(), self.due())


class Promotion(ABC):  # 策略:抽象基类
    """
    从Python3.4开始,声明抽象基类最简单的方式是子类化abc.ABC
    """

    @abstractmethod
    def discount(self, order: Order):
        """返回折扣金额"""


class FidelityPromo(Promotion):
    """
    第一个具体策略
    为积分为1000或以上的顾客提供5%折扣
    """

    def discount(self, order: Order):
        return order.total() * .05 if order.customer.fidelity >= 1000 else 0


class BulkItemPromo(Promotion):
    """
    第二个具体策略
    单个商品为20个或以上时提供10%折扣
    """

    def discount(self, order: Order):
        discount = 0
        for item in order.cart:
            if item.quantity >= 20:
                discount += item.total() * .01
        return discount


class LargeOrderPromo(Promotion):
    """
    第三个具体策略
    订单中不停商品达到10个或以上时提供7%折扣
    """

    def discount(self, order: Order):
        discount_items = {item.product for item in order.cart}
        if len(discount_items) >= 10:
            return order.total() * .07
        return 0


# 两个顾客:joe的积分是0,ann的积分是1000.
joe = Customer('John Doe', 0)
ann = Customer('Ann Smith', 1000)
# 有3个商品的购物车
cart = [LineItem('banana', 4, .5),
        LineItem('apple', 10, 1.5),
        LineItem('watermellon', 5, 5.0)]
print(Order(joe, cart, FidelityPromo()))  # <Order total: 42.00 due: 42.00> FidelityPromo没给joe提供折扣
print(Order(ann, cart, FidelityPromo()))  # <Order total: 42.00 due: 39.90> ann得到了折扣,因为她的积分超过了1000
# banana_cart中有30把香蕉和10个苹果
banana_cart = [LineItem('banana', 30, .5),
               LineItem('apple', 10, 1.5)]
print(Order(joe, banana_cart, BulkItemPromo()))  # <Order total: 30.00 due: 29.85> BulkItemPromo
# long_order中有10种不同的商品
long_order = [LineItem(str(item_code), 1, 1.0)
              for item_code in range(10)]
print(Order(joe, long_order, LargeOrderPromo()))  # <Order total: 10.00 due: 9.30>
print(Order(joe, cart, LargeOrderPromo()))  # <Order total: 42.00 due: 42.00>

"""
使用函数实现'策略'模式
上面的示例中,每个具体策略都是一个类,而且都只定义了一个方法,即discount.此外,策略实例没有状态.你可能会说,它们看起来像是普通的函数.
的确如此.下面把具体策略换成简单的函数.
"""


class Order:  # 上下文
    def __init__(self, customer: Customer, cart, promotion=None):
        self.__total = None
        self.customer = customer
        self.cart = list(cart)
        self.promotion = promotion

    def total(self):
        if self.__total is None:
            self.__total = sum(item.total() for item in self.cart)
        return self.__total

    def due(self):
        if self.promotion is None:
            discount = 0
        else:
            discount = self.promotion(self)  # 计算折扣只需要调用self.promotion()函数.
        return self.__total - discount

    def __repr__(self):
        fmt = '<Order total: {:.2f} due: {:.2f}>'
        return fmt.format(self.total(), self.due())


def fidelity_promo(order):
    """
    第一个具体策略
    为积分为1000或以上的顾客提供5%折扣
    """
    return order.total() * .05 if order.customer.fidelity >= 1000 else 0


def bulk_item_promo(order):
    """
        第二个具体策略
        单个商品为20个或以上时提供10%折扣
        """
    discount = 0
    for item in order.cart:
        if item.quantity >= 20:
            discount += item.total() * .1
    return discount


def large_order_promo(order):
    """
    第三个具体策略
    订单中不停商品达到10个或以上时提供7%折扣
    """
    distinct_items = {item.product for item in order.cart}
    if len(distinct_items) >= 10:
        return order.total() * .07
    return 0


# 两个顾客:joe的积分是0,ann的积分是1000.
joe = Customer('John Doe', 0)
ann = Customer('Ann Smith', 1000)
# 有3个商品的购物车
cart = [LineItem('banana', 4, .5),
        LineItem('apple', 10, 1.5),
        LineItem('watermellon', 5, 5.0)]
print(Order(joe, cart, fidelity_promo))  # <Order total: 42.00 due: 42.00> fidelity_promo没给joe提供折扣
print(Order(ann, cart, fidelity_promo))  # <Order total: 42.00 due: 39.90> ann得到了折扣,因为她的积分超过了1000
# banana_cart中有30把香蕉和10个苹果
banana_cart = [LineItem('banana', 30, .5),
               LineItem('apple', 10, 1.5)]
print(Order(joe, banana_cart, bulk_item_promo))  # <Order total: 30.00 due: 29.85>
# long_order中有10种不同的商品
long_order = [LineItem(str(item_code), 1, 1.0)
              for item_code in range(10)]
print(Order(joe, long_order, large_order_promo))  # <Order total: 10.00 due: 9.30>
print(Order(joe, cart, large_order_promo))  # <Order total: 42.00 due: 42.00>
"""
没必要在新键订单时实例化新的促销对象,函数拿来即用.
享元:享元时可共享的对象,可以同时在多个上下文中使用.共享是推荐的做法.这样不必在每个新的上下文中使用相同策略时不断新键具体策略对象,
从而减少消耗.在复杂的情况下,需要具体策略维护内部状态时,可能需要把'策略'和'享元'模式结合起来.但是,具体策略一般没有内部状态,只是处理
上下文中的数据.此时,一定要使用普通的函数,别去编写只有一个方法的类,再去实现另一个类声明的单函数接口.函数比用户定义的类的实例轻量,
而且无需使用'享元'模式,因为各个策略函数在Python编译模块时只会创建一次.普通的函数也是'可共享的对象,可以同时在多个上下文中使用.'
"""

# ------------------------------------选择最佳策略: 简单的方式------------------------------------
"""
上面,我们使用函数实现了'策略'模式,由此也出现了其他可能性.假设我们想创建一个'元策略',让它为指定的订单选择最佳折扣.
"""
promos = [fidelity_promo, bulk_item_promo, large_order_promo]


def best_promo(order: Order):
    """
    选择可用的最佳折扣
    """
    return max(promo(order) for promo in promos)


print(Order(joe, long_order, best_promo))  # <Order total: 10.00 due: 9.30>
print(Order(joe, banana_cart, best_promo))  # <Order total: 30.00 due: 28.50>
print(Order(ann, cart, best_promo))  # <Order total: 42.00 due: 39.90>
"""
上面的示例简单明了,promos是函数列表.习惯函数是一等对象后,自然而然就会构建那种数据结构存储函数.
虽然上面的示例可用,而且易于阅读,但是有些重复可能会导致不易察觉的缺陷:若想添加新的促销策略,
要定义相应的函数,还要记得把它添加到promos列表中;否则,当新促销函数显示地作为参数传给Order时,
它是可用的,但是best_promo不会考虑它.
"""

# ------------------------------------找出模块中的全部策略------------------------------------
"""
在Python中,模块也是一等对象,而且标准库提供了几个处理模块的函数.
globals()
    返回一个字典,表示当前的全局符号表.这个符号表始终针对当前模块(对函数或方法来说,是指定义它们的模块,而不是调用它们的模块)
下面的列表推导式,迭代globals()返回字典中的各个name,只选择以_promo结尾的名称,过滤掉best_promo自身,防止无限递归.
"""
promos_ = [globals()[name] for name in globals() if name.endswith('_promo') and name != 'best_promo']
import inspect
import funcs
"""
收集所有可用促销的另一种方法是,在一个单独的模块中保存所有策略函数,把best_promo排除在外.
下面的代码最大的变化是内省名为funcs的独立模块,构建策略函数列表.
inspect.getmembers函数用于获取对象(这里是funcs对象)的属性,第二个参数是可选的判断条件(一个布尔值函数).
我们使用的是inspect.isfunction,只获取模块中的函数.
"""
promos__ = [func for name, func in inspect.getmembers(funcs, inspect.isfunction)]
# [<function fidelity_promo at 0x0000029692F81F28>, <function bulk_item_promo at 0x00000296AB7587B8>,
# <function large_order_promo at 0x00000296AB758840>]
print(promos_)
# [<function bulk_item_promo at 0x00000296AB758A60>, <function fidelity_promo at 0x00000296AB7589D8>,
# <function large_order_promo at 0x00000296AB758AE8>]
print(promos__)

# ------------------------------------'命令'模式------------------------------------
"""
'命令'设计模式也可以通过把函数作为参数传递而简化.
'命令'模式的目的是解耦调用操作的对象(调用者)和提供实现的对象(接收者).在<<设计模式:可复用面向对象软件的基础>>所举的示例中,
调用者是图形应用程序中的菜单项,而接收者是被编辑的文档或应用程序本身.
这个模式的做法是,在二者之间放一个Command对象,让它实现只有一个方法(execute)的接口,调用接收者中的方法执行所需的操作.
这样,调用者无需了解接收者的接口,而且不同的接收者可用适应不同的Command子类.调用者有一个具体的命令,通过调用execute方法执行.
"""
