# 本实例实现一个二维向量类

from math import hypot


class Vector:

    def __init__(self, x: int = 0, y: int = 0):
        self.x = x
        self.y = y

    def __repr__(self):
        """
        内置函数repr(),用于把一个对象用字符串的形式表达出来以便辨认.
        返回的字符串应该准确无歧义,并且尽可能表达出如何用代码构建出这个被打印的对象.
        :return: str
        """
        return "Vector(%r, %r)" % (self.x, self.y)

    def __abs__(self):
        return hypot(self.x, self.y)

    def __bool__(self):
        """
        为了判定一个值x为真还是为假,python会调用bool(x),这个函数只能返回True或False.
        默认情况下,我们自己定义的类的实例总被人为是真的.除非这个类对__bool__或__len__函数有自己的定义.
        bool(x)的背后是调用x.__bool__()的结果;如果不存在__bool__方法,那么bool(x)会尝试调用x.__len__(),
        若返回0,则则bool()会返回False,否则返回True
        :return: bool
        """
        # return bool(abs(self))
        return True if self.x or self.y else False

    def __add__(self, other):
        """
        实现加法 + , 中缀运算符的基本原则是不改变操作对象,而是产出一个新的值
        :param other: Vector
        :return: Vector
        """
        x = self.x + other.x
        y = self.y + other.y
        return Vector(x, y)

    def __mul__(self, other: int):
        """
        实现向量的标量乘法
        :param other: int
        :return: Vector
        """
        return Vector(self.x * other, self.y * other)


if __name__ == '__main__':
    v1 = Vector(2, 4)
    v2 = Vector(1, 6)
    print(v1)
    print(v1 + v2)
    print(v1 * 3)
    print(abs(v1))
    print(bool(v1))
