from fastapi import FastAPI, Request
# jinja2 模板渲染
from fastapi.templating import Jinja2Templates
from tortoise.contrib.fastapi import HTTPNotFoundError, register_tortoise
from dao.models import Todo

app = FastAPI()
template = Jinja2Templates("pages")
register_tortoise(
    app,
    db_url="mysql://root:123456@47.99.83.221:3306/mynpb",
    modules={"models": ['dao.models']},
    add_exception_handlers=True,
)


@app.get("/")
async def user(username, req: Request):
    todos = await Todo.all()
    print(todos.content)
    # template.get_template("index.html")
    return template.TemplateResponse("index.html", context={"request": req, "username": todos})
