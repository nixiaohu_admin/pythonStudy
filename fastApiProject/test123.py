# coding=utf-8
# --coding--=utf-8

from selenium import webdriver
import time
import csv
from threading import Thread
import threading

def process(canshu):
    # lock = threading.Lock()
    # lock.acquire()
    # global driver
    driver = webdriver.Chrome()
    # browser.get('https://www.baidu.com')
    # time.sleep(30)
    driver.set_window_size(1000, 800)
    driver.get('https://auth.huaweicloud.com/authui/login.html#/login')
    print(canshu, "++" * 30)
    time.sleep(10)
    # 调整的iam登录页面
    # lock = threading.Lock()
    # lock.acquire()
    driver.find_element_by_xpath('//*[@id="IAMLinkDiv"]/span').click()

    # time.sleep(30)
    driver.implicitly_wait(20)
    driver.find_element_by_xpath('//*[@id="IAMAccountInputId"]').send_keys('loongcloud')
    driver.find_element_by_xpath('//*[@id="IAMUsernameInputId"]').send_keys('lyzdmaguoyang')
    driver.find_element_by_xpath('//*[@id="IAMPasswordInputId"]').send_keys('6157561-xxxxx')
    driver.find_element_by_xpath('//*[@id="btn_submit"]').click()
    time.sleep(30)
    # lock.release()

    print('执行run 函数')
    # 获取所有的安全组信息

    driver.get(
        'https://console.huaweicloud.com/vpc/?agencyId=e63d14d410dc44e2a4dd898501b51b83&region=cn-north-4&locale=zh-cn#/secGroups')
    time.sleep(5)
    print("==========",canshu)

    driver.implicitly_wait(10)
    # 获取进入下一页的标签
    next_page = driver.find_element_by_xpath('//*[@id="securityContent"]/div/div[2]/div/div/div[2]/a[9]')
    # 获取用于判断是否是最后一页的属性
    is_next_url = next_page.get_attribute("title")
    print(is_next_url)

    i = 0
    # 下一页和最后一页的区别
    while True:
        if is_next_url == "下一页":
            print("执行--safe_group 函数")
            # driver.get(
            #     'https://console.huaweicloud.com/vpc/?agencyId=e63d14d410dc44e2a4dd898501b51b83&region=cn-north-4&locale=zh-cn#/secGroups')
            time.sleep(5)

            tr_list = driver.find_elements_by_xpath('//*[@id="sec_group_list_table"]/tbody/tr')[0:]

            # 获取安全组名称和连接
            for haha in tr_list:
                safe_name = {}
                # item['id'] = (tr.text).split(" ")[0]
                # safe_name['safe_name_pro'] = (haha.text).split(" ")[0]
                elements = driver.find_element_by_link_text((haha.text).split(" ")[0])
                safe_name['link'] = elements.get_attribute('href')
                # print("link", safe_name['safe_name_pro'], safe_name['link'])

                print('link__', safe_name['link'])
                url1 = safe_name['link']
                js = f"window.open('{url1}')"
                driver.execute_script(js)
                time.sleep(5)
                print(driver.window_handles)
                driver.switch_to.window(driver.window_handles[-1])
                print("跳转页面", driver.current_window_handle)
                # safe_group_id()


                print('所有句柄', driver.window_handles)
                driver.switch_to.window(driver.window_handles[0])
                print('安全组页面', driver.current_window_handle)
            next_page.click()
            time.sleep(3)
            i += 1
            is_next_url = next_page.get_attribute("title")
            print("is next", is_next_url, i)
        else:
            print("执行--safe_group 函数")
            # driver.get(
            #     'https://console.huaweicloud.com/vpc/?agencyId=e63d14d410dc44e2a4dd898501b51b83&region=cn-north-4&locale=zh-cn#/secGroups')
            time.sleep(5)

            tr_list = driver.find_elements_by_xpath('//*[@id="sec_group_list_table"]/tbody/tr')[0:]

            # 获取安全组名称和连接
            for haha in tr_list:
                safe_name = {}
                # item['id'] = (tr.text).split(" ")[0]
                # safe_name['safe_name_pro'] = (haha.text).split(" ")[0]
                elements = driver.find_element_by_link_text((haha.text).split(" ")[0])
                safe_name['link'] = elements.get_attribute('href')
                # print("link", safe_name['safe_name_pro'], safe_name['link'])

                print('link__', safe_name['link'])
                url1 = safe_name['link']
                js = f"window.open('{url1}')"
                driver.execute_script(js)
                time.sleep(5)
                print(driver.window_handles)
                driver.switch_to.window(driver.window_handles[-1])
                print("跳转页面", driver.current_window_handle)
                # safe_group_id()


                print('所有句柄', driver.window_handles)
                driver.switch_to.window(driver.window_handles[0])
                print('安全组页面', driver.current_window_handle)
            break

    driver.close()
    driver.quit()


# 规则页面执行 入方向规则+ 出方向规则
# def safe_group_id():
#     # driver.get('https://console.huaweicloud.com/vpc/?agencyId=e63d14d410dc44e2a4dd898501b51b83&region=cn-north-4&locale=zh-cn#/secGroups/SGDetail?instanceId=a50b0c75-5d7c-4137-bdfa-86d432923513')
#     print('执行--safe_group_id函数')
#     # time.sleep(5)
#     driver.implicitly_wait(20)
#     # 选择入方向规则
#     driver.find_element_by_link_text('入方向规则').click()
#     time.sleep(5)
#     Tong_use_rule('入方向规则')
#
#     # 选择出方向规则
#     print("出方向规则")
#     driver.implicitly_wait(20)
#     driver.find_element_by_link_text('出方向规则').click()
#     time.sleep(5)
#     Tong_use_rule('出方向规则')
#     time.sleep(2)
#     driver.close()

def main():
    # 开启4个进程，传入爬取的页码范围
    thead_list = []
    t1 = Thread(target=process,args=(1,))
    t1.start()

    t2 = Thread(target=process,args=(2,))
    t2.start()

    thead_list.append(t1)
    thead_list.append(t2)
    for t in thead_list:
        t.join()

if __name__ == '__main__':
    s = time.time()
    main()
    e = time.time()
    print('总用时：',e-s)