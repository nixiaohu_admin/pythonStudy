from pydantic import BaseModel
from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Language(BaseModel):
    language_id: str = None
    language_name: str
    description: str = None
    year: int = 2010
    ranking: int = 1


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    tags: list = []


@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item):
    results = {"item_id": item_id, "item": item}
    return results


@app.post(path='/create_language')
async def create_language(language: Language):
    """
        cmd:      curl -H "Content-Type: application/json" -X POST -d "{\"language_name\": \"Python\", \"ranking\":9}" http://127.0.0.1:8000/create_language

        return : {"language_id":null,"language_name":"Python","description":null,"year":2010,"ranking":9}%


    :param language:
    :return:
    """
    print(language)
    return language
